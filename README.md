# TodoList

This is a simple todo list app that renders dinamically based on the response of embedded API.

### You can try this app directly on this [live demo](https://todo-list-tau-ten.vercel.app)

## It adds the ability to:

- Add a todo item/
  - If empty a random name will be generated
- Delete an item
- Toggle item status
- Search for any item based on the task name

## Getting Started

### Installation

- Clone this repo

### Install dependencies

- ```bash
  yarn
  ```

### Install API

- ```bash
  cd ./api
  yarn
  ```

### Run API

- ```bash
  yarn start
  ```

### Start frontend

- ```bash
  yarn start
  ```

### Run tests

- ```bash
  yarn test
  ```
