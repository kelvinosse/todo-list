var express = require('express')
const { v4: uuidv4 } = require('uuid')
const { isEmpty } = require('lodash')
let { list, suggestedTodoNames } = require('../utils')
var router = express.Router()

/* GET todo tasks listing. */
router.get('/', function (req, res, next) {
  res.send(list)
})

/* POST todo task. */
router.post('/', (req, res) => {
  if (isEmpty(req.body.title.trim())) {
    const randomTitle =
      suggestedTodoNames[Math.floor(Math.random() * suggestedTodoNames.length)]
    list.push({ id: uuidv4(), title: randomTitle, done: false })
  } else {
    list.push({ ...req.body, id: uuidv4(), done: false })
  }
  res.json(list)
})

/* PUT todo task. Toggles the current status */
router.put('/', (req, res) => {
  list = list.map((item) => ({
    ...item,
    done: item.id === req.body.id ? !item.done : item.done
  }))
  res.json(list)
})
/* DELETE todo task. */
router.delete('/', (req, res) => {
  list = list.filter(({ id }) => id !== req.body.id)
  res.json(list)
})
module.exports = router
