const { v4: uuidv4 } = require('uuid')

const suggestedTodoNames = [
  'Take a bubble bath',
  'Pull a harmless prank on one of your friends',
  'Take a nap',
  'Organize a cluttered drawer',
  'Have a football scrimmage with some friends',
  'Bake pastries for you and your neighbor',
  'Configure two-factor authentication on your accounts',
  'Memorize a favorite quote or poem',
  "Start a blog for something you're passionate about",
  'Learn how to french braid hair',
  'Make a scrapbook with pictures of your favorite memories',
  'Start a daily journal',
  'Learn and play a new card game',
  'Start a garden',
  'Go see a Broadway production',
  'Draw something interesting',
  'Pot some plants and put them around your house',
  'Take a spontaneous road trip with some friends',
  'Make a couch fort',
  'Go to a music festival with some friends',
  'Go to the gym',
  'Learn woodworking',
  'Start a family tree',
  'Start a collection',
  'Find a charity and donate to it',
  'Watch Doraemon 😸'
]

const list = [
  {
    id: uuidv4(),
    title: 'Create todo list',
    done: true
  }
]

module.exports = {
  suggestedTodoNames,
  list
}
