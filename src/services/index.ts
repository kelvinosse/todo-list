import { API_PATH } from '../utils'

/** PUT API call to toggle a task status */
export const toggleTaskStatus = (id: string) => {
  return fetch(`${API_PATH}/list`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ id })
  })
}

/** DELETE API call to remove a task from the todo list */
export const deleteTaskItem = (id: string) => {
  return fetch(`${API_PATH}/list`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ id })
  })
}

/** POST API call to add a task to the todo list */
export const addTaskItem = (title: string) => {
  return fetch(`${API_PATH}/list`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ title })
  })
}

/** GET API call to remove a task */
export const getTaskItems = () => {
  return fetch(`${API_PATH}/list`).then((res) => res.json())
}
