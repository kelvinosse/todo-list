import { css } from '@emotion/react'

const styles = {
  container: css`
    * {
      font-family: 'Montserrat', sans-serif;
    }

    section {
      display: grid;
      margin-bottom: 5rem;
      @media (min-width: 800px) {
        place-content: center;
      }
    }

    h1 {
      text-align: center;
      margin: 1rem;
      color: #5e5e5e;
      font-size: 3rem;
    }
  `
}

export default styles
