/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useCallback, useEffect, useState } from 'react'

import styles from './styles'
import Welcome from './components/Welcome'
import SearchBar from './components/SearchBar'
import TaskList from './components/TaskList'
import TaskInput from './components/TaskInput'
import { fromToggledStatusTasks, ListItem } from './utils'
import { getTaskItems } from './services'

const App = () => {
  const [tasks, setTasks] = useState<ListItem[]>([] as ListItem[])
  const [filteredTasks, setFilteredTasks] = useState<ListItem[]>(
    [] as ListItem[]
  )
  const [searchInput, setSearchInput] = useState<string>('')

  const loadList = useCallback(async () => {
    const items = await getTaskItems()
    setTasks(items)
  }, [])

  const updateTasks = (list: ListItem[]) => {
    setFilteredTasks(list)
    setTasks(list)
  }

  const onToggleTask = (id: string) => {
    setFilteredTasks(fromToggledStatusTasks(filteredTasks, id))
    setTasks(fromToggledStatusTasks(tasks, id))
  }

  const onSearchText = (query: string) => {
    setSearchInput(query)
    if (searchInput !== '') {
      const filteredItems = tasks.filter((item) =>
        Object.values(item)
          .join('')
          .toLowerCase()
          .includes(searchInput.toLowerCase())
      )
      setFilteredTasks(filteredItems)
    } else {
      setFilteredTasks(tasks)
    }
  }

  const pendingTasksCount =
    searchInput.length > 1
      ? filteredTasks.filter(({ done }) => !done).length
      : tasks.filter(({ done }) => !done).length

  useEffect(() => {
    loadList().catch((err) =>
      console.error('An unexpected error ocurred: ', err)
    )
  }, [loadList])

  return (
    <div css={styles.container}>
      <h1>TodoList</h1>
      <section>
        <Welcome pendingTasksCount={pendingTasksCount} />
        <SearchBar onSearchText={onSearchText} />
        <TaskInput onNewTask={updateTasks} />
        <TaskList
          tasks={searchInput.length > 1 ? filteredTasks : tasks}
          onDeleteTask={updateTasks}
          onToggleTask={onToggleTask}
        />
      </section>
    </div>
  )
}

export default App
