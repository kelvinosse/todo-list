export const API_PATH = 'http://localhost:9000'

/** Helper function to return the toggled tasks status based on matching id */
export const fromToggledStatusTasks = (
  tasks: ListItem[],
  id: string
): ListItem[] => {
  return tasks.map((item) => ({
    ...item,
    done: item.id === id ? !item.done : item.done
  }))
}

/** Helper type to list item structure */
export type ListItem = {
  id: string
  title: string
  done: boolean
}
