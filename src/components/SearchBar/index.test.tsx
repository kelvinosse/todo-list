import React from 'react'
import { render, screen } from '@testing-library/react'
import SearchBar from './index'

describe('SearchBar', () => {
  it('should render without problems', () => {
    const props = {
      onSearchText: () => {}
    }
    render(<SearchBar {...props} />)
    expect(screen.getByRole('textbox')).toBeInTheDocument()
  })
})
