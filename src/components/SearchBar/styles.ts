import { css } from '@emotion/react'

const styles = {
  searchBar: css`
    display: grid;
    svg {
      vertical-align: middle;
    }

    input {
      padding: 1rem;
      width: 15rem;
      border: none;
      outline: none;
      background: transparent;
    }

    input::placeholder {
      color: #90a0b7;
    }

    input::-ms-input-placeholder {
      color: #90a0b7;
    }
  `
}

export default styles
