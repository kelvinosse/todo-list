/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react'
import styles from './styles'
import { ReactComponent as SearchIcon } from '../../assets/search.svg'
import { ChangeEvent, useState } from 'react'

type Props = {
  onSearchText: (query: string) => void
}

const SearchBar = ({ onSearchText }: Props) => {
  const [query, setQuery] = useState<string>('')

  const onSubmit = (e: React.FormEvent) => {
    e.preventDefault()
    onSearchText(query)
  }

  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    setQuery(e.target.value)
    onSearchText(e.target.value)
  }

  return (
    <div css={styles.searchBar}>
      <form onSubmit={onSubmit}>
        <SearchIcon />
        <input
          type="text"
          placeholder="Search for any task"
          onChange={onChange}
          value={query}
        />
      </form>
    </div>
  )
}

export default SearchBar
