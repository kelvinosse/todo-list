import React from 'react'
import { render, screen } from '@testing-library/react'
import TaskList from './index'

describe('TaskList', () => {
  const props = {
    tasks: [{ id: '1', title: 'title', done: false }],
    onDeleteTask: () => {},
    onToggleTask: () => {}
  }
  it('should render task list heading', () => {
    render(<TaskList {...props} />)
    expect(screen.getByText('Title')).toBeInTheDocument()
  })

  it('should render checkbox elements', () => {
    render(<TaskList {...props} />)
    expect(screen.getByRole('checkbox')).toBeInTheDocument()
  })
})
