import { css } from '@emotion/react'

const styles = {
  taskList: css`
    display: grid;
    grid-template-columns: 3em 5em 20em;
    grid-auto-flow: row;
    gap: 1em;
    > div {
      display: flex;
    }

    span {
      font-size: 1.3rem;
    }

    h2 {
      font-weight: 600;
    }
  `,
  checkbox: css`
    width: 100%;
    height: 1.3rem;
    border: 1px solid #615454;
    border-radius: 5px;
    background-color: #1e9cea;
    cursor: pointer;
  `,
  notFound: css`
    display: grid;
    grid-template-columns: 30em;
  `
}

export default styles
