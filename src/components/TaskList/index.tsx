/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react'
import React, { Fragment } from 'react'
import { ListItem } from '../../utils'
import TaskElement from '../TaskElement'
import styles from './styles'

type Props = {
  tasks: ListItem[]
  onDeleteTask?: (list: ListItem[]) => void
  onToggleTask?: (id: string) => void
}

const HeaderRow = () => (
  <Fragment>
    <div>
      <h2>№</h2>
    </div>
    <div>
      <h2>Status</h2>
    </div>
    <div>
      <h2>Title</h2>
    </div>
  </Fragment>
)

const TaskList = ({ tasks, onDeleteTask, onToggleTask }: Props) => {
  const taskElements = tasks?.map(({ id, title, done }, index) => (
    <TaskElement
      key={index}
      index={index + 1}
      id={id}
      title={title}
      done={done}
      onDeleteTask={onDeleteTask}
      onToggleTask={onToggleTask}
    />
  ))

  return (
    <Fragment>
      {tasks.length > 0 ? (
        <div css={styles.taskList}>
          <HeaderRow />
          {taskElements}
        </div>
      ) : (
        <div css={styles.notFound}>
          <h2>No tasks found 😮</h2>
          <i>Try searching or create a new one.</i>
        </div>
      )}
    </Fragment>
  )
}

export default TaskList
