import React from 'react'
import { render, screen } from '@testing-library/react'
import TaskElement from './index'

describe('TaskElement', () => {
  const props = {
    id: '1',
    index: 1,
    title: 'title',
    done: false,
    onDeleteTask: () => {},
    onToggleTask: () => {}
  }
  it('should render task element input', () => {
    render(<TaskElement {...props} />)
    expect(screen.getByRole('checkbox')).toBeInTheDocument()
  })
  it('should render task element title', () => {
    render(<TaskElement {...props} />)
    expect(screen.getByText('title')).toBeInTheDocument()
  })
})
