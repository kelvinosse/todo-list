/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react'

import { Fragment } from 'react'
import styles from './styles'
import { ReactComponent as TrashIcon } from '../../assets/trash.svg'
import { ListItem } from '../../utils'
import { deleteTaskItem, toggleTaskStatus } from '../../services'

type Props = {
  id: string
  index: number
  title: string
  done: boolean
  onDeleteTask?: (list: ListItem[]) => void
  onToggleTask?: (id: string) => void
}

const TaskElement = ({
  id,
  index,
  title,
  done,
  onDeleteTask,
  onToggleTask
}: Props) => {
  const deleteTask = async (id: string) => {
    await deleteTaskItem(id)
      .then(async (res) => {
        const newList = await res.json()
        if (onDeleteTask) onDeleteTask(newList)
      })
      .catch((err) => console.error('An unexpected error ocurred: ', err))
  }

  const toggleTask = async (id: string) => {
    await toggleTaskStatus(id)
      .then(async () => {
        if (onToggleTask) onToggleTask(id)
      })
      .catch((err) => console.error('An unexpected error ocurred: ', err))
  }

  return (
    <Fragment>
      <div>
        <TrashIcon css={styles.deleteIcon} onClick={() => deleteTask(id)} />
        <span>{index}</span>
      </div>
      <div>
        <input
          css={styles.checkbox}
          type="checkbox"
          checked={done}
          onChange={() => toggleTask(id)}
        />
      </div>
      <div css={styles.titleContainer}>
        <span css={done && styles.strikeThroughText}>{title}</span>
      </div>
    </Fragment>
  )
}

export default TaskElement
