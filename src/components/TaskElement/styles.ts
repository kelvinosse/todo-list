import { css } from '@emotion/react'

const styles = {
  checkbox: css`
    width: 100%;
    height: 1.3rem;
    border: 1px solid #615454;
    border-radius: 5px;
    cursor: pointer;
  `,
  deleteIcon: css`
    margin-right: 0.5rem;
    cursor: pointer;
  `,
  strikeThroughText: css`
    text-decoration: line-through;
  `,
  titleContainer: css`
    word-break: break-word;
  `
}

export default styles
