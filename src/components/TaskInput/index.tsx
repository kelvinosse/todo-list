/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react'
import React, { useState, FormEvent, ChangeEvent } from 'react'
import { addTaskItem } from '../../services'
import { ListItem } from '../../utils'
import styles from './styles'

type Props = {
  onNewTask: (list: ListItem[]) => void
}

const TaskInput = ({ onNewTask }: Props) => {
  const [taskInput, setTaskInput] = useState('')

  const onSubmit = async (e: FormEvent) => {
    e.preventDefault()
    await addTaskItem(taskInput)
      .then(async (res) => {
        const list = await res.json()
        onNewTask(list)
        setTaskInput('')
      })
      .catch((err) => console.error('An unexpected error ocurred: ', err))
  }

  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    setTaskInput(e.target.value)
  }

  return (
    <div css={styles.taskInput}>
      <form onSubmit={onSubmit}>
        <button>+</button>
        <input
          type="text"
          placeholder="Add New Task"
          onChange={onChange}
          value={taskInput}
        />
      </form>
    </div>
  )
}

export default TaskInput
