import React from 'react'
import { render, screen } from '@testing-library/react'
import TaskInput from './index'

describe('TaskInput', () => {
  const props = {
    onNewTask: () => {}
  }
  it('should render task input', () => {
    render(<TaskInput {...props} />)
    expect(screen.getByRole('textbox')).toBeInTheDocument()
  })
})
