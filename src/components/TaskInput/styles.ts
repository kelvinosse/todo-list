import { css } from '@emotion/react'

const styles = {
  taskInput: css`
    form {
      margin-top: 1rem;
    }
    button {
      background-color: #7aa7ea;
      border: none;
      border-radius: 20%;
      color: white;
      cursor: pointer;
      font-size: 1.2rem;
      text-align: center;
      text-decoration: none;
    }

    input {
      margin-left: 1rem;
      font-size: 1.2rem;
      border: none;
      outline: none;
      background: transparent;
    }

    input::placeholder {
      color: #7aa7ea;
    }

    input::-ms-input-placeholder {
      color: #7aa7ea;
    }
  `
}

export default styles
