/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react'

import { ReactComponent as ComputerEmoji } from '../../assets/emoji.svg'
import styles from './styles'

type Props = {
  pendingTasksCount: number
}

type PendingText = {
  [key: string]: string
}

const Welcome = ({ pendingTasksCount }: Props) => {
  const textTypes: PendingText = {
    0: 'No tasks pending',
    1: `${pendingTasksCount} task pending`
  }

  return (
    <div css={styles.container}>
      <ComputerEmoji />
      <div>
        <h2>Hi 👋</h2>
        <h3>
          {textTypes[pendingTasksCount] || `${pendingTasksCount} tasks pending`}
        </h3>
      </div>
    </div>
  )
}

export default Welcome
