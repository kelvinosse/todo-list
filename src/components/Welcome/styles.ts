import { css } from '@emotion/react'

const styles = {
  container: css`
    display: flex;
    svg {
      align-self: center;
      margin-right: 1rem;
    }

    h2 {
      margin-bottom: 0;
      color: #5e5e5e;
    }

    h3 {
      font-weight: 500;
      margin-top: 0;
      color: #ef6161;
    }

    div {
      flex-direction: column;
    }
  `
}

export default styles
