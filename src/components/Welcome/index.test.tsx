import React from 'react'
import { render, screen } from '@testing-library/react'
import Welcome from './index'

describe('Welcome', () => {
  const props = {
    pendingTasksCount: 0
  }

  it('should render welcome header', () => {
    render(<Welcome {...props} />)
    expect(screen.getByText('Hi 👋')).toBeInTheDocument()
  })

  it('should render no pending tasks', () => {
    render(<Welcome {...props} />)
    expect(screen.getByText('No tasks pending')).toBeInTheDocument()
  })
})
